package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Detail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Button btn_fb = findViewById(R.id.btn_fb);
        Button btn_back = findViewById(R.id.btn_back);


        btn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent fb = new Intent(Intent.ACTION_VIEW);
                fb.setData(Uri.parse("https://www.facebook.com/profile.php?id=100010173965311"));
                startActivity(Intent.createChooser(fb,"Open with"));
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(Detail.this, MainActivity.class);
                startActivity(back);
            }
        });

    }
}